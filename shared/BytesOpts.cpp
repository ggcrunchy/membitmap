/*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:
*
* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*
* [ MIT license: http://www.opensource.org/licenses/mit-license.php ]
*/

#include "Bytes.h"
#include <algorithm>
#include <cstring>

// Constructor
BytesOpts::ColumnIter::ColumnIter (RowIter & iter) : mRows(iter), mColumn(iter.mOpts.mX1), mPixel(iter.mData)
{
}

// Constructor
BytesOpts::ColumnIter::ColumnIter (RowIter & iter, const unsigned char * end) : mRows(iter), mColumn(iter.mOpts.mX1), mPixel(iter.mData)
{
	size_t count = iter.mOpts.GetWidth();

    if (end < mPixel) end = mPixel;
    
	size_t range = (end - mPixel) / iter.mBPP;

	if (range < count) mColumn += count - range;
}

// Advance to the next column
void BytesOpts::ColumnIter::Next (void)
{
	++mColumn;

	mPixel += mRows.mBPP;
}

// Set some bytes in the pixel pointed at by the column
void BytesOpts::ColumnIter::Set (unsigned char * bytes)
{
	for (int i = 0; i < mRows.mWanted; ++i) mPixel[mRows.mOpts.mOffset + i] = bytes[i];
}

// Set some bytes in the pixel pointed at by the column, unless the color key is found 
void BytesOpts::ColumnIter::SetColorKey (unsigned char * bytes)
{
	auto opts = mRows.mOpts;
	unsigned int key = opts.mColorKeyInt;

	opts.mColorKeyInt = 0;

	for (int i = 0; i < mRows.mWanted; ++i) opts.mColorKeyBytes[i] = bytes[i];

	if (opts.mColorKeyInt != key) Set(bytes);

	opts.mColorKeyInt = key;
}

// Set some (grayscale) bytes in the pixel pointed at by the column
void BytesOpts::ColumnIter::SetGray (unsigned char * bytes)
{
	unsigned char gray = *bytes;

	for (int i = 0; i < 3; ++i) mPixel[i] = gray;
}

// Set some (grayscale) bytes in the pixel pointed at by the column, unless the color key is found
void BytesOpts::ColumnIter::SetGrayColorKey (unsigned char * bytes)
{
	auto opts = mRows.mOpts;
	unsigned int key = opts.mColorKeyInt;

	opts.mColorKeyInt = 0;
	opts.mColorKeyBytes[0] = *bytes;

	if (opts.mColorKeyInt != key) SetGray(bytes);

	opts.mColorKeyInt = key;
}

// Constructor
BytesOpts::RowIter::RowIter (BytesOpts & opts, MemBitmap * bitmap, int bpp, int wanted) : mOpts(opts), mBPP(bpp), mRow(opts.mY1)
{
	mData = PointToData(bitmap->data, opts.mX1, opts.mY1, bitmap->w, mBPP, &mStride);
	mWanted = std::min(wanted, mBPP);
}

// Kick off a column iterator
BytesOpts::ColumnIter BytesOpts::RowIter::Columns (void)
{
	return ColumnIter(*this);
}
		
// Kick off a column iterator and diminish write count
BytesOpts::ColumnIter BytesOpts::RowIter::Columns (const unsigned char * end)
{
	return ColumnIter(*this, end);
}

// Advance to the next row
void BytesOpts::RowIter::Next (void)
{
	++mRow;

	mData += mStride;
}

// Auxiliary reader for loads
static int ReadOpts (lua_State * L)
{
	BytesOpts * pOpts = (BytesOpts *)lua_touserdata(L, 1);

	pOpts->Load(L, (MemBitmap *)lua_touserdata(L, 2), 3);

	return 0;
}

// Constructor
BytesOpts::BytesOpts (lua_State * L, int opts, MemBitmap * bitmap, bool set_bytes) : mOK(true), mColorKey(false), mGray(false), mSetBytes(set_bytes), mX1(0), mY1(0), mLeft(0), mTop(0), mInfoAt(-1), mOffset(0), mColorKeyInt(0)
{
	// Set appropriate defaults.
	mFormat = bitmap->format;
	mX2 = bitmap->w - 1;
	mY2 = bitmap->h - 1;
	mWidth = bitmap->w;

	// If no options were provided, use the defaults.
	if (!lua_istable(L, opts)) return;

	// When setting bytes, we might want be assigning from a source narrower than the bitmap,
	// so start as too-thin-to-exist and work up from there.
	if (set_bytes) mWidth = -1;

	// Call out to a helper load function to catch any errors.
	lua_pushcfunction(L, ReadOpts);	// ..., ReadOpts
	lua_pushlightuserdata(L, this);	// ..., this
	lua_pushlightuserdata(L, bitmap);	// ..., ReadOpts, this, bitmap
	lua_pushvalue(L, opts);	// ..., ReadOpts, this, bitmap, opts

	mOK = lua_pcall(L, 3, 0, 0) == 0; // ...[, err]

	// When setting bytes, account for regions that veered outside the bitmap.
	if (set_bytes) AdjustSetRegion(bitmap);

	// Clamp the region against the bitmap. If the region is empty, flag it in the width.
	if (mX1 < bitmap->w && mY1 < bitmap->h && mX2 >= 0 && mY2 >= 0)
	{
		mX1 = std::max(mX1, 0);
		mY1 = std::max(mY1, 0);
		mX2 = std::min(mX2, bitmap->w - 1);
		mY2 = std::min(mY2, bitmap->h - 1);
	}

	else mWidth = 0;

	// If info is requested, create a table to receive it (or use the one provided).
	if (!mOK) return;

	lua_getfield(L, opts, "get_info");	// ..., info?

	if (lua_toboolean(L, -1) != 0)
	{
		mInfoAt = opts;

		if (!lua_istable(L, -1))
		{
			lua_pop(L, 1);	// ...
			lua_newtable(L);// ..., info
		}
	}

	// At this point we are done with any options, so hijack their stack slot.
	lua_replace(L, opts);	// ..., info?, ...
}

// Kick off a row iterator
BytesOpts::RowIter BytesOpts::Rows (MemBitmap * bitmap)
{
	return RowIter(*this, bitmap, bitmap->GetEffectiveBPP(), 0);
}

// Kick off a row iterator, allowing for wanted bits per pixel
BytesOpts::RowIter BytesOpts::Rows (MemBitmap * bitmap, int wanted)
{
	return RowIter(*this, bitmap, bitmap->GetEffectiveBPP(), wanted);
}

// Get an optional coordinate
int BytesOpts::GetCoord (lua_State * L, int opts, const char * name, int dim, bool bHigh)
{
	lua_getfield(L, opts, name);// ..., name

	int coord = luaL_optint(L, -1, bHigh ? dim : 1) - 1;

	lua_pop(L, 1);	// ...

	return coord;
}

// Helper to return get / set bytes result, plus any info if requested
int BytesOpts::Return (lua_State * L)
{
	if (!WantsInfo()) return 1;

	lua_pushvalue(L, mInfoAt);	// ..., bytes / ok, info

	return 2;
}

// Add format and rect to info
void BytesOpts::AddFormatAndRect (lua_State * L)
{
	Member ints[] = {
		{ "x1", mX1 }, { "y1", mY1 }, { "x2", mX2 }, { "y2", mY2 }
	};

	for (auto i : ints)
	{
		lua_pushinteger(L, i.mValue + 1);	// ..., value
		lua_setfield(L, mInfoAt, i.mName);	// ..., info = { name = value }, ...
	}

	switch (mFormat)
	{
	case kExternalBitmapFormat_Mask:
		lua_pushliteral(L, "mask");	// ..., "mask"
		break;
	case kExternalBitmapFormat_RGB:
		lua_pushliteral(L, "rgb");	// ..., "rgb"
		break;
	default:
		lua_pushliteral(L, "rgba");	// ..., "rgba"
	}

	lua_setfield(L, mInfoAt, "format");	// ..., info = { format = format }, ...
}

// Fix regions when setting bytes
void BytesOpts::AdjustSetRegion (MemBitmap * bitmap)
{
	mWidth = std::max(mWidth, GetWidth());

	if (mX1 < 0) mLeft -= mX1;
	if (mY1 < 0) mTop -= mY1;
}

// Get the optional custom format
void BytesOpts::GetFormat (lua_State * L, int opts)
{
	lua_getfield(L, opts, "format");// ..., format

	if (!lua_isnil(L, -1))
	{
		// When setting bytes, process any requests for grayscale.
		if (mSetBytes && mFormat != kExternalBitmapFormat_Mask)
		{
			lua_pushliteral(L, "grayscale");// ..., format, "grayscale"

			mGray = lua_equal(L, -2, -1) != 0;

			lua_pop(L, 1);	// ..., format
		}

		if (mGray) mFormat = kExternalBitmapFormat_Mask;

		else
		{
			// Begin by servicing any single-component format request, ensuring that the
			// bitmap can even accommodate it.
			const char * colors[] = { "red", "green", "blue", "alpha" };

			int offset = -1;

			for (int i = 0; i < 4 && offset < 0; ++i, lua_pop(L, 1))
			{
				lua_pushstring(L, colors[i]);	// ..., format, name

				if (lua_equal(L, -2, -1)) offset = i;
			}

			luaL_argcheck(L, offset < 0 || offset < CoronaExternalFormatBPP(mFormat), -1, "Component unavailable in bitmap");

			if (offset >= 0)
			{
				mFormat = kExternalBitmapFormat_Mask;
				mOffset = offset;
			}

			// Otherwise, the format must be one of Corona's own.
			else
			{
				const char * names[] = { "mask", "rgb", "rgba" };
				CoronaExternalBitmapFormat formats[] = { kExternalBitmapFormat_Mask, kExternalBitmapFormat_RGB, kExternalBitmapFormat_RGBA };

				mFormat = formats[luaL_checkoption(L, -1, NULL, names)];
			}
		}
	}

	lua_pop(L, 1);	// ...
}

// Load body
void BytesOpts::Load (lua_State * L, MemBitmap * bitmap, int opts)
{
	// Get the rect in the bitmap.
	mX1 = GetCoord(L, opts, "x1", bitmap->w, false);
	mY1 = GetCoord(L, opts, "y1", bitmap->h, false);
	mX2 = GetCoord(L, opts, "x2", bitmap->w, true);
	mY2 = GetCoord(L, opts, "y2", bitmap->h, true);

	if (mX1 > mX2) std::swap(mX1, mX2);
	if (mY1 > mY2) std::swap(mY1, mY2);

	lua_getfield(L, opts, "stride");// ..., stride

	mWidth = luaL_optint(L, -1, mWidth);

	lua_pop(L, 1);	// ...

	// Check for requests to interpret the bytes in a specific format.
	GetFormat(L, opts);

	// If setting bytes, assign any color key (adhering to the format).
	if (mSetBytes)
	{
		lua_getfield(L, opts, "colorkey");	// ..., colorkey

		ByteReader key(L, -1);

		if (key.mBytes && key.mCount > 0)
		{
			memcpy(mColorKeyBytes, key.mBytes, std::min(int(key.mCount), CoronaExternalFormatBPP(mFormat)));

			mColorKey = true;
		}

		lua_pop(L, 1);	// ...
	}
}

// Point to the start of a given pixel
unsigned char * BytesOpts::PointToData (unsigned char * start, int x, int y, int w, int bpp, int * stride)
{
	int istride = w * bpp;

	if (stride) *stride = istride;

	return start + y * istride + x * bpp;
}