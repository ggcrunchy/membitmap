/*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:
*
* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*
* [ MIT license: http://www.opensource.org/licenses/mit-license.php ]
*/

#include "CoronaGraphics.h"
#include "PluginMemBtm.h"
#include "Bitmap.h"
#include "Bytes.h"
#include <algorithm>

// Helper to inject a "true" into the result
static int OK (lua_State * L, BytesOpts & opts)
{
	lua_pushboolean(L, 1);	// bitmap, ..., opts / info, ok

	return opts.Return(L);	// bitmap, ...[, opts], ok[, info]
}

// Byte setter body
template<void (BytesOpts::ColumnIter::*memfn)(unsigned char *)> void SetBytesCore (lua_State * L, MemBitmap * btm, const void * bytes, BytesOpts & opts, size_t n)
{
	int bpp = CoronaExternalFormatBPP(opts.mFormat), stride, xend = (int(n) % opts.mWidth == 0 ? opts.mX2 : opts.mX1) + 1;
	auto pdata = BytesOpts::PointToData((unsigned char *)bytes, opts.mLeft, opts.mTop, opts.mWidth, bpp, &stride);
	auto end = BytesOpts::PointToData(btm->data, xend, opts.mY2, btm->w, btm->GetEffectiveBPP(), NULL);

	for (auto rows = opts.Rows(btm, bpp); !rows.Done(); rows.Next(), pdata += stride)
	{
		auto pbytes = pdata;

		for (auto cols = rows.Columns(end); !cols.Done(); cols.Next(), pbytes += bpp) (cols.*memfn)(pbytes);
	}
}

// Sets bytes, in the case where padding components are added
static int AuxSetBytes (lua_State * L, MemBitmap * btm, const void * bytes, BytesOpts & opts, size_t n)
{
	if (n > 0)
	{
		if (!opts.mColorKey)
		{
			if (!opts.mGray) SetBytesCore<&BytesOpts::ColumnIter::Set>(L, btm, bytes, opts, n);
			
			else SetBytesCore<&BytesOpts::ColumnIter::SetGray>(L, btm, bytes, opts, n);
		}

		else
		{
			if (!opts.mGray) SetBytesCore<&BytesOpts::ColumnIter::SetColorKey>(L, btm, bytes, opts, n);
			
			else SetBytesCore<&BytesOpts::ColumnIter::SetGrayColorKey>(L, btm, bytes, opts, n);
		}
	}

	return OK(L, opts);
}

// Assign bytes to the memory bitmap
// TODO: Docs
int MemBitmap_SetBytes (lua_State * L)
{
	MemBitmap * btm = (MemBitmap *)CoronaExternalGetUserData(L, 1);

	if (btm != NULL)
	{
		ByteReader bytes(L, 2);	// bitmap, bytes, ...[, error]

		if (bytes.mBytes)
		{
			BytesOpts opts(L, 3, btm, true);

			if (opts.mOK)
			{
				// Get the pixel count: this will either be the bitmap area or the number of
				// (whole) pixels available in the bytes, whichever is lesser. Given this
				// amount, try to pare off any extra rows.
				size_t n = std::min(bytes.mCount / CoronaExternalFormatBPP(opts.mFormat), size_t(btm->w * btm->h));

				if (opts.mWidth != 0) // width = 0 when region is empty
				{
					int to_row = (n + opts.mWidth - 1) / opts.mWidth - 1;

					opts.mY2 = std::min(opts.mY1 + to_row, opts.mY2);
				}

				// If requested, populate relevant info fields.
				if (opts.WantsInfo()) opts.AddFormatAndRect(L);	// bitmap, bytes[, opts / info]

				if (opts.mWidth != 0)
				{
					// If the component counts match, we only care what region we want from
					// the bitmap. This is easy when the full rect is requested: blast all
					// bytes into it.
					if (opts.CanCopy(btm) && !opts.mColorKey) memcpy(btm->data, bytes.mBytes, n * btm->GetEffectiveBPP());

					// Otherwise, we must assign pixel by pixel.
					else AuxSetBytes(L, btm, bytes.mBytes, opts, n);
				}

				return OK(L, opts);	// bitmap, bytes, ok[, info]
			}
		}
	}

	else lua_pushliteral(L, "Missing bitmap");	// ..., error

	lua_pushnil(L);	// ..., error, nil
	lua_insert(L, -2);	// ..., nil, error

	return 2;
}