/*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:
*
* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*
* [ MIT license: http://www.opensource.org/licenses/mit-license.php ]
*/

#include "CoronaGraphics.h"
#include "PluginMemBtm.h"
#include "Bitmap.h"
#include "Bytes.h"

// Gets bytes, in the case where padding components are added
static int GetAugmentedBytes (lua_State * L, MemBitmap * btm, BytesOpts & opts, int nwanted, int true_bpp)
{
	luaL_Buffer B;

	luaL_buffinit(L, &B);

	// Go through the rows, accumulating columns one by one inside the requested range. In
	// each instance, pad the column with some suitable default bytes.
	unsigned char pad[4] = { 0, 0, 0, 255 };

	for (auto rows = opts.Rows(btm); !rows.Done(); rows.Next())
	{
		for (auto cols = rows.Columns(); !cols.Done(); cols.Next())
		{
			luaL_addlstring(&B, cols, true_bpp); // n.b. no offset since these only apply to masks (one byte, thus never augmented)
			luaL_addlstring(&B, (const char *)pad + true_bpp, nwanted - true_bpp);
		}
	}

	// Return the amalgamated fragment data, along with any info.
	luaL_pushresult(&B);// ...[, opts / info], bytes

	return opts.Return(L);	// ..., bytes[, info]
}

// Get bytes, in the case where components are being dropped
static int GetReducedBytes (lua_State * L, MemBitmap * btm, BytesOpts & opts, int nwanted)
{
	luaL_Buffer B;

	luaL_buffinit(L, &B);

	// Go through the rows, accumulating columns one by one inside the requested range.
	for (auto rows = opts.Rows(btm); !rows.Done(); rows.Next())
	{
		for (auto cols = rows.Columns(); !cols.Done(); cols.Next()) luaL_addlstring(&B, cols + opts.mOffset, nwanted);
	}

	// Return the amalgamated fragment data, along with any info.
	luaL_pushresult(&B);// ...[, opts / info], bytes

	return opts.Return(L);	// ..., bytes[, info]
}

// Extract bytes from the memory bitmap
// TODO: Docs
int MemBitmap_GetBytes (lua_State * L)
{
	MemBitmap * btm = (MemBitmap *)CoronaExternalGetUserData(L, 1);

	if (btm != NULL)
	{
		BytesOpts opts(L, 2, btm, false);

		if (opts.mOK)
		{
			// If requested, populate relevant info fields.
			if (opts.WantsInfo()) opts.AddFormatAndRect(L);	// bitmap[, opts / info], ...

			// In the case of an empty region, return a zero-length byte stream.
			if (opts.mWidth == 0)
			{
				lua_pushliteral(L, "");	// bitmap[, opts / info], ..., ""

				return opts.Return(L);	// bitmap, ""[, info]
			}

			// If the component counts differ (either by request or because of representation
			// quirks), either augment or reduce the data accordingly, returning it.
			int nwanted = CoronaExternalFormatBPP(opts.mFormat), true_bpp = btm->GetEffectiveBPP();

			if (true_bpp > nwanted) return GetReducedBytes(L, btm, opts, nwanted);	// bitmap, bytes[, info]
			if (true_bpp < nwanted) return GetAugmentedBytes(L, btm, opts, nwanted, true_bpp);	// bitmap, bytes[, info]

			// Since the component counts match, we now only care what region we want from the
			// bitmap. This is easy when the full rect is requested: return all bytes intact.
			if (opts.CanCopy(btm)) lua_pushlstring(L, (const char *)btm->data, true_bpp * btm->w * btm->h);	// bitmap[, opts / info], bytes

			// Otherwise, we must accumulate a line at a time.
			else
			{
				luaL_Buffer B;

				luaL_buffinit(L, &B);

				// For each row, get all columns in the requested range. At the end, merge them.
				int w = opts.GetWidth() * true_bpp;

				for (auto rows = opts.Rows(btm); !rows.Done(); rows.Next()) luaL_addlstring(&B, rows, w);

				luaL_pushresult(&B);// bitmap[, opts / info], bytes
			}

			// Return the bytes. If requested, append some info.
			return opts.Return(L);	// bitmap, bytes[, info]
		}
	}

	else lua_pushliteral(L, "Missing bitmap");	// ..., error

	lua_pushnil(L);	// ..., error, nil
	lua_insert(L, -2);	// ..., nil, error

	return 2;
}