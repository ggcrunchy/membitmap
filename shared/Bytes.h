/*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:
*
* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*
* [ MIT license: http://www.opensource.org/licenses/mit-license.php ]
*/

#ifndef BYTES_H
#define BYTES_H

#include "CoronaLua.h"
#include "CoronaGraphics.h"
#include "Bitmap.h"
#include "ByteReader.h"

// State for various options used by non-default getBytes() and setBytes() calls
struct BytesOpts {
	bool mOK;	// Loading succeeded?
	bool mColorKey, mGray, mSetBytes;	// Has color key? Is grayscale? Setting bytes?
	CoronaExternalBitmapFormat mFormat;	// Format of data to get or set
	int mX1, mY1, mX2, mY2;	// Region to get or set
	int mLeft, mTop, mWidth;// Assignment offset, when setting bytes; data width
	int mOffset;// Offset used to read off color-specific bytes
	int mInfoAt;// Stack location of info table
	union {
		unsigned int mColorKeyInt;	// Integer representation of color key...
		unsigned char mColorKeyBytes[4];// ...and byte representation
	};

	struct Member {
		const char * mName;	// Field name, in Lua table
		int mValue;	// Value of opts member
	};

	class RowIter;

	class ColumnIter {
		RowIter & mRows;// Rows to which these columns belong
		int mColumn;// Current column
		unsigned char * mPixel;	// Data pointed to by column

	public:
		bool Done (void) const { return mColumn > mRows.mOpts.mX2; }
		operator const char * (void) const { return (const char *)mPixel; }
		void Next (void);
		void Set (unsigned char * bytes);
		void SetColorKey (unsigned char * bytes);
		void SetGray (unsigned char * bytes);
		void SetGrayColorKey (unsigned char * bytes);

		ColumnIter (RowIter & iter);
		ColumnIter (RowIter & iter, const unsigned char * end);
	};

	class RowIter {
		BytesOpts & mOpts;	// Current byte options
		int mBPP, mStride;	// Bits-per-pixel and stride of bitmap
		int mRow;	// Current row
		int mWanted;// Wanted bits-per-pixel
		unsigned char * mData;	// Data pointed to by row

		friend class ColumnIter;

	public:
		bool Done (void) const { return mRow > mOpts.mY2; }
		operator const char * (void) const { return (const char *)mData; }

		RowIter (BytesOpts & opts, MemBitmap * bitmap, int bpp, int wanted);

		ColumnIter Columns (void);
		ColumnIter Columns (const unsigned char * end);

		void Next (void);
	};

	BytesOpts (lua_State * L, int opts, MemBitmap * bitmap, bool set_bytes);

	RowIter Rows (MemBitmap * bitmap);
	RowIter Rows (MemBitmap * bitmap, int wanted);

	bool CanCopy (MemBitmap * bitmap) const { return mFormat == bitmap->GetRepFormat() && !IsCustomRect(bitmap); }
	bool IsCustomRect (MemBitmap * bitmap) const { return mX1 > 0 || mY1 > 0 || mX2 < bitmap->w - 1 || mY2 < bitmap->h - 1; }
	bool WantsInfo (void) const { return mInfoAt > 0; }

	int GetCoord (lua_State * L, int opts, const char * name, int dim, bool bHigh);
	int GetWidth (void) const { return mX2 - mX1 + 1; }
	int Return (lua_State * L);

	void AddFormatAndRect (lua_State * L);
	void AdjustSetRegion (MemBitmap * bitmap);
	void GetFormat (lua_State * L, int opts);
	void Load (lua_State * L, MemBitmap * bitmap, int opts);

	static unsigned char * PointToData (unsigned char * start, int x, int y, int w, int bpp, int * stride);
};

int MemBitmap_GetBytes (lua_State * L);
int MemBitmap_SetBytes (lua_State * L);

#endif